# Demultiplexing procedure with `cutadapt`

`cutadapt` can be used to demultiplex paired-end reads with unique tag combinations. This is done following [cutadapt documentation](https://cutadapt.readthedocs.io/en/stable/guide.html#demultiplexing-paired-end-reads-with-combinatorial-dual-indexes) for demultiplexing paired-end reads with combinatorial dual indexes. Note that this feature of `cutadapt` is only available in `v2.4` or above.

## Demultiplexing the reads

The cutadapt function needs as imputs, two `.fasta` files containing the forward and reverse sequences of each tag (or tag + primer).
For instance, here are my two files containing the 16S tag+primer.

**Note:** To speedup the process, sequences should be anchored in the fasta file (with `^tag`).

    head tag_primers_fwd_16S.fasta 
    
        >F1_16S
        ^CCTAAACTACGGGTGBCAGCMGCCGCGGTAA
        >F2_16S
        ^TGCAGATCCAACGTGBCAGCMGCCGCGGTAA
        >F3_16S
        ^CCATCACATAGGGTGBCAGCMGCCGCGGTAA
        >F4_16S
        ^GTGGTATGGGAGTGTGBCAGCMGCCGCGGTAA
        >F5_16S
        ^ACTTTAAGGGTGTGTGBCAGCMGCCGCGGTAA

    head tag_primers_rev_16S.fasta
    
        >R1_16S
        ^CCTAAACTACGGGGACTACHVGGGTWTCTAAT
        >R2_16S
        ^TGCAGATCCAACGGACTACHVGGGTWTCTAAT
        >R3_16S
        ^CCATCACATAGGGGACTACHVGGGTWTCTAAT
        >R4_16S
        ^GTGGTATGGGAGAGGACTACHVGGGTWTCTAAT
        >R5_16S
        ^ACTTTAAGGGTGAGGACTACHVGGGTWTCTAAT


The pooled sequences should be in one R1 and one R2 files in `.fastq.gz` 
format (probably works with `.fastq` and `.fasta` too?):

* `run23_S1_L001_R1_001.fastq.gz`
* `run23_S1_L001_R2_001.fastq.gz`

**IMOPORTANT**: In this run, the forward primer is found on the R2 reads, and the 
reverse primer on the R1 reads. This is often the case, it worth checking it on a few 
sequences before demultiplexing.

Then we can run cutadapt v2.4 or above (I asked for an update on SCW):

```
cutadapt \
    -e 0.15 --no-indels \
    -g file:tag_primers_fwd_16S.fasta \
    -G file:tag_primers_rev_16S.fasta \
    -o {name1}-{name2}.R2.fastq.gz -p {name1}-{name2}.R1.fastq.gz \
    run23_S1_L001_R2_001.fastq.gz run23_S1_L001_R1_001.fastq.gz
```
This will create a new R1 and R2 file for every possible tag combination, an copy the 
matching paired-end seuqences in it, **after trimming the primer and tag**. For the first 
tag combination, we obtain:

* `F1_16S-R1_16S.R1.fastq.gz` 
* `F1_16S-R1_16S.R2.fastq.gz`

The files `unknown-unknown.R1.fastq.gz` `unknown-unknown.R2.fastq.gz` contain the sequences
without any tag+primer. In the case of a run with multiple primers (16S and ITS in this one)
these files contain the reads with the second primers. We can demultiplex in a second time 
with `tag_primers_fwd_ITS.fasta` `tag_primers_rev_ITS.fasta` containing the second tag+primer 
sequences (don't forget to create a new directory, or be sure that your primers all have 
unique names).

````
cutadapt \
    -e 0.15 --no-indels \
    -g file:$input_dir/tag_primers_fwd_ITS.fasta \
    -G file:$input_dir/tag_primers_rev_ITS.fasta \
    -o ITS_sequences/{name1}-{name2}.R2.fastq.gz -p ITS_sequences/{name1}-{name2}.R1.fastq.gz \
    16S_sequences/unknown-unknown.R2.fastq.gz 16S_sequences/unknown-unknown.R1.fastq.gz``
````

## Renaming files according to the sample names.

In a second time, we can now rename files according to the sample names corresponding to 
the tag combination. To do this, I use `mmv`, but it is not installed on SCW, and an alternative
can probably be found. For now I just do it in local.

I produced in R a file containing the correspondance of names looking like this:

    head run23_16S_rename_index_local.txt

        F5_16S-R4_16S.R1.fastq.gz Summer_AS1_PCR1.R1.fastq.gz
        F5_16S-R5_16S.R1.fastq.gz Summer_AS1_PCR2.R1.fastq.gz
        F5_16S-R6_16S.R1.fastq.gz Summer_AS1_PCR3.R1.fastq.gz
        F5_16S-R7_16S.R1.fastq.gz Summer_AH_PCR1.R1.fastq.gz
        F5_16S-R8_16S.R1.fastq.gz Summer_AH_PCR2.R1.fastq.gz
        F5_16S-R9_16S.R1.fastq.gz Summer_AH_PCR3.R1.fastq.gz

And then simply run in the directory containing the demultiplexed sequences:

    mmv < run23_16S_rename_index_local.txt

That's it! We now have a file per sample, containing the demultiplexed reads, **without primers**.

*Note that for short target sequences, the reverse complement of primers may still be present on the 3'
of the reads, and needs to be subsequently removed.*
